const binance = require('./api');
const data = require('./key');

binance.options({
  APIKEY: data.APIKEY,
  APISECRET: data.APISECRET,
	test: true,
});

binance.marketBuy("VENBTC", 100, function(response) {
	console.log("Limit Buy response", response);
	console.log("order id: " + response.orderId);
});